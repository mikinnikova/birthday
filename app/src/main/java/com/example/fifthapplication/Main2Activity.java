package com.example.fifthapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {

    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        result = (TextView) findViewById(R.id.result);

        long date = getIntent().getExtras().getLong("date");
        String name = getIntent().getStringExtra("name");
        String zodiak = getIntent().getStringExtra("zodiak");

        date = (((date / 60_000) / 60) / 24) / 365;
        long days = date * 365;
        long sec = days * 24 * 60;
        result.setText(name + " your age is: " + String.valueOf(date) + "\n you lived: " + days + " days\n and " + sec + " seconds!\n" + " your zodiak: " + zodiak);

        //Toast.makeText(this, getIntent().getStringExtra("date"), Toast.LENGTH_LONG).show();
    }
}
