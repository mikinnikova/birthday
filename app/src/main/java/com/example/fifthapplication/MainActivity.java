package com.example.fifthapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    EditText vvod;
    EditText name;
    Button okey;
    Date yourDate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        vvod = (EditText) findViewById(R.id.editText2);
        name = (EditText) findViewById(R.id.editText);
        okey = (Button) findViewById(R.id.btn_ok);

        okey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Main2Activity.class);
//                if (vvod.getText().toString().contains(".")) {
//                    String[] birth = vvod.getText().toString().split(".");
//
//                    int year1 = Integer.parseInt((birth[3]));
//                    int year = 2017-year1;

                String birth = vvod.getText().toString();

                SimpleDateFormat curFormater = new SimpleDateFormat("dd.MM.yyyy");

                try {
                    yourDate = curFormater.parse(birth);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                long milisecOfBirth = yourDate.getTime();
                long milis = System.currentTimeMillis() - milisecOfBirth;

                int month = yourDate.getMonth();
                String znak = null;

                switch (month) {
                    case 0:
                        if (yourDate.getDate() >= 1 && yourDate.getDate() <= 20) {
                            znak = "Kozerog";
                        } else if (yourDate.getDate() >= 21 && yourDate.getDate() <= 31) {
                            znak = "Vodoley";
                        }
                        break;
                    case 1:
                        if (yourDate.getDate() >= 1 && yourDate.getDate() <= 18) {
                            znak = "Vodoley";
                        } else if (yourDate.getDate() >= 19 && yourDate.getDate() <= 29) {
                            znak = "Ribi";
                        }
                        break;
                    case 2:
                        if (yourDate.getDate() >= 1 && yourDate.getDate() <= 20) {
                            znak = "Ribi";
                        } else if (yourDate.getDate() >= 21 && yourDate.getDate() <= 31) {
                            znak = "Oven";
                        }
                        break;
                    case 3:
                        if (yourDate.getDate() >= 1 && yourDate.getDate() <= 19) {
                            znak = "Oven";
                        } else if (yourDate.getDate() >= 20 && yourDate.getDate() <= 30) {
                            znak = "Telets";
                        }
                        break;
                    case 4:
                        if (yourDate.getDate() >= 1 && yourDate.getDate() <= 20) {
                            znak = "Telets";
                        } else if (yourDate.getDate() >= 21 && yourDate.getDate() <= 31) {
                            znak = "Bliztetsi";
                        }
                        break;
                    case 5:
                        if (yourDate.getDate() >= 1 && yourDate.getDate() <= 21) {
                            znak = "Bliznetsi";
                        } else if (yourDate.getDate() >= 22 && yourDate.getDate() <= 30) {
                            znak = "Rak";
                        }
                        break;
                    case 6:
                        if (yourDate.getDate() >= 1 && yourDate.getDate() <= 22) {
                            znak = "Rak";
                        } else if (yourDate.getDate() >= 23 && yourDate.getDate() <= 31) {
                            znak = "Lev";
                        }
                        break;
                    case 7:
                        if (yourDate.getDate() >= 1 && yourDate.getDate() <= 22) {
                            znak = "Lev";
                        } else if (yourDate.getDate() >= 23 && yourDate.getDate() <= 31) {
                            znak = "Deva";
                        }
                        break;
                    case 8:
                        if (yourDate.getDate() >= 1 && yourDate.getDate() <= 22) {
                            znak = "Deva";
                        } else if (yourDate.getDate() >= 23 && yourDate.getDate() <= 30) {
                            znak = "Vesi";
                        }
                        break;
                    case 9:
                        if (yourDate.getDate() >= 1 && yourDate.getDate() <= 23) {
                            znak = "Vesi";
                        } else if (yourDate.getDate() >= 24 && yourDate.getDate() <= 31) {
                            znak = "Skorpion";
                        }
                        break;
                    case 10:
                        if (yourDate.getDate() >= 1 && yourDate.getDate() <= 22) {
                            znak = "Skorpion";
                        } else if (yourDate.getDate() >= 23 && yourDate.getDate() <= 30) {
                            znak = "Strelets";
                        }
                        break;
                    case 11:
                        if (yourDate.getDate() >= 1 && yourDate.getDate() <= 21) {
                            znak = "Strelets";
                        } else if (yourDate.getDate() >= 22 && yourDate.getDate() <= 31) {
                            znak = "Kozerog";
                        }
                        break;
                }
                intent.putExtra("date", milis);
                intent.putExtra("name", name.getText().toString());
                intent.putExtra("zodiak", znak);
                    startActivity(intent);
                }
        });
    }
    }

